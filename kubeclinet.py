#!/usr/bin/python
# Script name: usage_info.py
from kubernetes import client, config


def list_nodes():
    config.load_kube_config()

    api = client.CustomObjectsApi()
    k8s_node = api.list_cluster_custom_object("metrics.k8s.io", "v1beta1", "nodes")

    string_text = "OUTPUT \n"
    for stats in k8s_node['items']:
        string_text += str("Node Name: %s\tCPU: %s\tMemory: %s \n" % (
            stats['metadata']['name'], stats['usage']['cpu'], stats['usage']['memory']))

    text = str(string_text)
    return text


def list_pod(namespace):
    v1 = client.CoreV1Api()
    print("Listing pods with their IPs:")
    ret = v1.list_pod_for_all_namespaces(watch=False)
    # print(ret.items)
    for i in ret.items:
        if namespace == i.metadata.namespace:
            print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))


