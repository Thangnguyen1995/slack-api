FROM python:3

RUN mkdir /root/.kube
COPY ./config /root/.kube/
WORKDIR /usr/src/app

RUN /usr/local/bin/python3 -m pip install --upgrade pip
RUN /usr/local/bin/python3 -m pip install slackclient slackeventsapi Flask kubernetes

COPY . .

CMD [ "python", "-u", "./main.py" ]