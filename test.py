# import os
# import subprocess
# import jc.parsers.ls
# # out = subprocess.Popen(['ls'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
# out = subprocess.Popen(['ls'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
# stdout, stderr = out.communicate()
# text = str(stdout).replace('\\', '\').replace(''', '')
# print(stdout)
# # print(out)
#
#
# # print("--------------")
# # print(stdout)
# # print("---------------")
# # print(stderr)

import json
import sys
import random
import requests
from kubernetes import client, config
from kubernetes import config , client
from kubernetes.client import Configuration
from kubernetes.client.api import core_v1_api
from kubernetes.client.rest import ApiException
from kubernetes.stream import stream
import re
# config.load_kube_config()

# api = client.CustomObjectsApi()
# k8s_node = api.list_cluster_custom_object("metrics.k8s.io", "v1beta1", "nodes")
#
# string_text = "OUTPUT \n"
# for stats in k8s_node['items']:
#     string_text += str("Node Name: %s\tCPU: %s\tMemory: %s \n" % (
#         stats['metadata']['name'], stats['usage']['cpu'], stats['usage']['memory']))
#
# text = str(string_text)

from kubernetes import client, config

# Configs can be set in Configuration class directly or using helper utility
config.load_kube_config()

v1 = client.CoreV1Api()
print("Listing pods with their IPs:")
ret = v1.list_pod_for_all_namespaces(watch=False)
# print(ret.items)
for i in ret.items:
    if "cpo-dev" == i.metadata.namespace:
        print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))


# from kubernetes import client, config, watch
#
# # Configs can be set in Configuration class directly or using helper utility
# config.load_kube_config()
#
# v1 = client.CoreV1Api()
# count = 10
# w = watch.Watch()
# for event in w.stream(v1.list_namespace, _request_timeout=60):
#     print("Event: %s %s" % (event['type'], event['object'].metadata.name))
#     count -= 1
#     if not count:
#         w.stop()
#
# print("Ended.")